import React from 'react'
import '../css/Suggestions.css'

const Suggestions = () => {
  return (
    <>
      <div className='suggestion'>Take a walk for a change.</div>
    </>
  )
}

export default Suggestions
